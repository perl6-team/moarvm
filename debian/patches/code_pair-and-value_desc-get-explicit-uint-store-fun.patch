From: Timo Paulssen <timonator@perpetuum-immobile.de>
Date: Fri, 4 Oct 2024 03:34:32 +0200
Subject: code_pair and value_desc get explicit uint store funcs

---
 src/6model/containers.c | 18 ++++++++++++++++--
 src/core/callsite.c     | 10 +++++++++-
 src/core/callsite.h     |  1 +
 3 files changed, 26 insertions(+), 3 deletions(-)

diff --git a/src/6model/containers.c b/src/6model/containers.c
index 1522685..33fa783 100644
--- a/src/6model/containers.c
+++ b/src/6model/containers.c
@@ -67,6 +67,12 @@ static void code_pair_store_i(MVMThreadContext *tc, MVMObject *cont, MVMint64 va
     code_pair_store_internal(tc, cont, r, MVM_callsite_get_common(tc, MVM_CALLSITE_ID_OBJ_INT));
 }
 
+static void code_pair_store_u(MVMThreadContext *tc, MVMObject *cont, MVMuint64 value) {
+    MVMRegister r;
+    r.u64 = value;
+    code_pair_store_internal(tc, cont, r, MVM_callsite_get_common(tc, MVM_CALLSITE_ID_OBJ_UINT));
+}
+
 static void code_pair_store_n(MVMThreadContext *tc, MVMObject *cont, MVMnum64 value) {
     MVMRegister r;
     r.n64 = value;
@@ -114,7 +120,7 @@ static const MVMContainerSpec code_pair_spec = {
     code_pair_fetch_s,
     code_pair_store,
     code_pair_store_i,
-    (void *)code_pair_store_i, /* FIXME need a code_pair_store_u but lacking tests showing this need */
+    code_pair_store_u,
     code_pair_store_n,
     code_pair_store_s,
     code_pair_store,
@@ -247,6 +253,14 @@ static void value_desc_cont_store_i(MVMThreadContext *tc, MVMObject *cont, MVMin
     value_desc_cont_store(tc, cont, boxed);
 }
 
+static void value_desc_cont_store_u(MVMThreadContext *tc, MVMObject *cont, MVMuint64 value) {
+    MVMObject *boxed;
+    MVMROOT(tc, cont) {
+        boxed = MVM_repr_box_uint(tc, MVM_hll_current(tc)->uint_box_type, value);
+    }
+    value_desc_cont_store(tc, cont, boxed);
+}
+
 static void value_desc_cont_store_n(MVMThreadContext *tc, MVMObject *cont, MVMnum64 value) {
     MVMObject *boxed;
     MVMROOT(tc, cont) {
@@ -371,7 +385,7 @@ static const MVMContainerSpec value_desc_cont_spec = {
     value_desc_cont_fetch_s,
     value_desc_cont_store,
     value_desc_cont_store_i,
-    (void *)value_desc_cont_store_i, /* FIXME need a value_desc_cont_store_u but lacking tests showing this need */
+    value_desc_cont_store_u,
     value_desc_cont_store_n,
     value_desc_cont_store_s,
     value_desc_cont_store_unchecked,
diff --git a/src/core/callsite.c b/src/core/callsite.c
index 9304303..6ef82bc 100644
--- a/src/core/callsite.c
+++ b/src/core/callsite.c
@@ -20,6 +20,9 @@ static MVMCallsite    obj_obj_callsite = { obj_obj_arg_flags, 2, 2, 2, 0, 0, NUL
 static MVMCallsiteEntry obj_int_flags[] = { MVM_CALLSITE_ARG_OBJ,
                                             MVM_CALLSITE_ARG_INT };
 static MVMCallsite    obj_int_callsite = { obj_int_flags, 2, 2, 2, 0, 0, NULL };
+static MVMCallsiteEntry obj_uint_flags[] = { MVM_CALLSITE_ARG_OBJ,
+                                            MVM_CALLSITE_ARG_UINT };
+static MVMCallsite    obj_uint_callsite = { obj_uint_flags, 2, 2, 2, 0, 0, NULL };
 
 static MVMCallsiteEntry obj_num_flags[] = { MVM_CALLSITE_ARG_OBJ,
                                             MVM_CALLSITE_ARG_NUM };
@@ -66,6 +69,8 @@ void MVM_callsite_initialize_common(MVMThreadContext *tc) {
     MVM_callsite_intern(tc, &ptr, 0, 1);
     ptr = &obj_int_callsite;
     MVM_callsite_intern(tc, &ptr, 0, 1);
+    ptr = &obj_uint_callsite;
+    MVM_callsite_intern(tc, &ptr, 0, 1);
     ptr = &obj_num_callsite;
     MVM_callsite_intern(tc, &ptr, 0, 1);
     ptr = &obj_str_callsite;
@@ -103,6 +108,8 @@ MVM_PUBLIC MVMCallsite * MVM_callsite_get_common(MVMThreadContext *tc, MVMCommon
             return &obj_obj_str_callsite;
         case MVM_CALLSITE_ID_OBJ_OBJ_OBJ:
             return &obj_obj_obj_callsite;
+        case MVM_CALLSITE_ID_OBJ_UINT:
+            return &obj_uint_callsite;
         default:
             MVM_exception_throw_adhoc(tc, "get_common_callsite: id %d unknown", id);
     }
@@ -336,7 +343,8 @@ static int is_common(MVMCallsite *cs) {
            cs == &obj_num_callsite      ||
            cs == &int_int_callsite      ||
            cs == &obj_obj_str_callsite  ||
-           cs == &obj_obj_obj_callsite;
+           cs == &obj_obj_obj_callsite  ||
+           cs == &obj_uint_callsite;
 }
 void MVM_callsite_cleanup_interns(MVMInstance *instance) {
     MVMCallsiteInterns *interns = instance->callsite_interns;
diff --git a/src/core/callsite.h b/src/core/callsite.h
index 399227b..bc368b3 100644
--- a/src/core/callsite.h
+++ b/src/core/callsite.h
@@ -40,6 +40,7 @@ typedef enum {
     MVM_CALLSITE_ID_INT_INT,
     MVM_CALLSITE_ID_OBJ_OBJ_STR,
     MVM_CALLSITE_ID_OBJ_OBJ_OBJ,
+    MVM_CALLSITE_ID_OBJ_UINT,
 } MVMCommonCallsiteID;
 
 /* A callsite entry is just one of the above flags. */
